﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonITESOApp
{
    public class CreditCard
    {
        public string Number { get; set; }
        public string Valid { get; set; }
    }
}
